package com.example.instagram_like_listview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Main extends Activity {

    ListView listView;
    ListAdapter adapter;
    TextView textViewGlobal;

    public String GetPrevious(int num){
        for (int i=num;i>=0;i--){
            if (!adapter.getItem(i).Title.isEmpty()){
                return adapter.getItem(i).Title;
            }
        }
        return "";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        listView=(ListView)findViewById(R.id.listView);
        textViewGlobal=(TextView)findViewById(R.id.textViewGlobal);
        ArrayList<ListItem> data=new ArrayList<>();

        data.add(new ListItem("Заголовок 1","Элемент 1"));
        data.add(new ListItem("","Элемент 2"));
        data.add(new ListItem("","Элемент 3"));
        data.add(new ListItem("","Элемент 4"));
        data.add(new ListItem("","Элемент 5"));
        data.add(new ListItem("","Элемент 6"));
        data.add(new ListItem("Заголовок 2","Элемент 8"));
        data.add(new ListItem("","Элемент 9"));
        data.add(new ListItem("","Элемент 10"));
        data.add(new ListItem("","Элемент 11"));
        data.add(new ListItem("","Элемент 12"));
        data.add(new ListItem("","Элемент 13"));
        data.add(new ListItem("Заголовок 3","Элемент 14"));
        data.add(new ListItem("","Элемент 15"));
        data.add(new ListItem("","Элемент 16"));
        data.add(new ListItem("","Элемент 17"));
        data.add(new ListItem("","Элемент 18"));
        data.add(new ListItem("","Элемент 19"));

        adapter=new ListAdapter(this,data);
        listView.setAdapter(adapter);

        textViewGlobal.setText(GetPrevious(0));

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mScrollState = scrollState;
            }

            @Override
            public void onScroll(AbsListView list, int firstItem, int visibleCount, int totalCount) {
                if (mScrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE)
                    return;

                for (int i=0; i < visibleCount; i++) {
                    View listItem = list.getChildAt(i);
                    if (listItem == null)
                        break;

                    TextView title = (TextView) listItem.findViewById(R.id.title);

                    int topMargin = 0;
                    if (i == 0) {
                        int top = listItem.getTop();
                        int height = listItem.getHeight();
                        if (top < 0)
                            topMargin = title.getHeight() < (top + height) ? -top : (height - title.getHeight());

                        if (title.getVisibility() == View.VISIBLE) {
                            textViewGlobal.setText(title.getText());
                        }
                    }
                    if (i == 1) {
                        if (title.getVisibility() == View.VISIBLE) {
                            textViewGlobal.setText(GetPrevious(firstItem));
                        }
                    }

                    ((ViewGroup.MarginLayoutParams) title.getLayoutParams()).topMargin = topMargin;
                    listItem.requestLayout();
                }
            }
        });
    }
}
