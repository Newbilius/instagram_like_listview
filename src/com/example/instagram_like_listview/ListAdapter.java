package com.example.instagram_like_listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {

    ArrayList<ListItem> object;
    Context ctx;
    LayoutInflater lInflater;

    public ListAdapter(Context context,ArrayList<ListItem> Object){
        ctx = context;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        object=Object;
    }

    @Override
    public int getCount() {
        return object.size();
    }

    @Override
    public ListItem getItem(int i) {
        return object.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) view = lInflater.inflate(R.layout.list_item, viewGroup, false);

        ListItem item=getItem(i);

        TextView title=(TextView) view.findViewById(R.id.title);
        TextView name=(TextView) view.findViewById(R.id.content);

        title.setText(item.Title);
        name.setText(item.Name);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        if (item.Title.isEmpty()){
            title.setVisibility(View.GONE);

            layoutParams.setMargins(0, 40, 0, 0);
            name.setLayoutParams(layoutParams);
        }else{
            title.setVisibility(View.VISIBLE);
            layoutParams.setMargins(0, 80, 0, 0);
            name.setLayoutParams(layoutParams);
        }

        return view;
    }
}
